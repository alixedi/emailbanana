chrome.extension.sendMessage({}, function(response) {
    var readyStateCheckInterval = setInterval(function() {
    if (document.readyState === "complete") {
        clearInterval(readyStateCheckInterval);

        // ----------------------------------------------------------
        // This part of the script triggers when page is done loading
        console.log("Hello. This message was sent from scripts/inject.js");
        // ----------------------------------------------------------

        // @alixedi
        /* 
         *  Our little app need to intervene on 2 views: Compose and Sent
         *  We check if its either one of those and call the handlers.
         */

        // clever shit? checking for change in anchor so that i dont have to 
        // check for all the kb shortcuts...
        $(window).on('hashchange', function() {

            // this is derived from the title - same everywhere
            var from = $('title').text().split('-')[1].trim();

            // check for compose anchor
            if(window.location.hash === '#page=Compose') {

                // when the user presses the send button
                $('#SendMessage').on('click', function (e) {
                    e.preventDefault();
                    var query = {
                        from: from,
                        to: $('#toCP').find('.cp_ctBtn').map(function() { return $(this).attr('title').trim(); }).get().join(),
                        subject: $('#fSubject').val().trim(),
                        total: $('.cp_ctBtn').length,
                        count: $('.cp_ctBtn').length
                    };
                    var url = 'http://evening-hollows-2191.herokuapp.com/EmailBanana/create/?' + $.param(query);
                    $.get(url.replace('%E2%80%8F', ''), function(data) {
                        var src = 'http://evening-hollows-2191.herokuapp.com/EmailBanana/read/' + data.id + '.jpg';
                        var html = '<img src="' + src + '" width=100 height=100 />';
                        $('.RichText').contents().find('body').append(html);
                        $('#SendMessage').off('click');
                        //$('#SendMessage').trigger('click');
                    });
                    return false;
                });
            }
            
            // check for sent anchor
            else if(window.location.hash === '#fid=flsent' || (window.location.hash === '' && window.location.search === '?fid=flsent')) {
                $.each($('.c-MessageRow'), function () {
                    var query = {
                        from: from,
                        to: $(this).find('.FmSender > span').text().trim(),
                        subject: $(this).find('.Sb > a').text().trim(),
                    };
                    var url = 'http://evening-hollows-2191.herokuapp.com/EmailBanana/find/?' + $.param(query);
                    var message = $(this);
                    $.get(url.replace('%E2%80%8F', ''), function(data) {
                        if (data.length > 0) {
                            var zcount = (data[0].count < 0)? 0 : data[0].count;
                            var readby = data[0].total - zcount;
                            $(message[0]).find('.Sb > a').prepend('<strong>[' + readby + '/' + data[0].total + ']</strong>');
                        }
                    });
                });
            }
        });
        
        $(window).trigger('hashchange');

    }
    }, 10);
});