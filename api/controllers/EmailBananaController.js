/**
 * EmailBananaController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

fs = require('fs');

module.exports = {
    
  read: function (req, res) {
    var id = parseInt(req.param('id').split('.')[0], 10);
	EmailBanana.findOne(id).exec(function (err, email) {
      if (err) return res.send(err,500);
      if (!email) return res.send("No other email with that id exists!", 404);
      //if (email.read) return res.send("The email is already read!", 403);

      // Read the email
      email.count = email.count - 1;

      // Persist the change
      email.save(function (err) {
        if (err) return res.send(err,500);

        // Report back with the new state of the chicken
        var img = fs.readFileSync('./assets/images/banana.jpg');
        res.writeHead(200, {'Content-Type': 'image/jpg' });
        res.end(img, 'binary');
        //res.json(email);
      });
    });
  },

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to EmailBananaController)
   */
  _config: {}

  
};
